# *_*coding:utf-8*_*
import requests

class DoubanScript(object):
    def __init__(self):
        self.base_url = "https://movie.douban.com/j/new_search_subjects?sort=U&range=0,10&tags=&start=20&genres=%E5%8A%A8%E4%BD%9C"
        self.header = {"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36"}
    # 发送请求
    def send(self):
        response = requests.get(self.base_url, headers=self.header)
        data = response.content
        return data
    # 保存
    def save(self, data):
        with open ("action.json", "wb") as f:
            f.write(data)\
    # 调度
    def run(self):
      data = self.send()
      self.save(data)
if __name__ == '__main__':
    dd = DoubanScript()
    dd.run()